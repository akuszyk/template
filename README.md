# Template Latex Book
This repo contains a template document structure and CI pipeline for Latex books that result in a versioned PDF being generated.

## Usage
Simply copy this repo and customise the following files:

* Rename `template.tex` to a name of your choice.
* Modify the `\title{}` element of `template.tex` to a title of your choice.

That's it, start writing!
